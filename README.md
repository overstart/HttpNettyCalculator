### HttpNettyCalculator

netty5的学习示例

### 使用方法

1. 直接运行CalcServer.java中的main函数
2. 或者先打包后运行
 * mvn package (打包jar文件)
 * java -jar HttpNettyCalculator-1.0-SNAPSHOT.jar (运行jar文件，默认开启8080端口)

### 请求地址

1. 求第n为斐波那契数
http://localhost:8080/fibonacci?n=41

2. 求s到e的累加和
http://localhost:8080/accumulation?s=1&e=100

### 关于netty5
netty5很早以前就被官方废弃, 见https://github.com/netty/netty/issues/4466, 本程序仅仅是以学习为目的, 顺手更新了几个bug 