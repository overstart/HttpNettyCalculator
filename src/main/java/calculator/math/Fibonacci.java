package calculator.math;

import java.util.concurrent.RecursiveTask;

/**
 * Created by liyf on 2015/3/30 0030.
 * 计算从 n位数的斐波那契数
 */
public class Fibonacci extends RecursiveTask<Long> {
    private static final long serialVersionUID = 7875142223684511653L;
    private final long n;

    public Fibonacci(long n) {
        this.n = n;
    }

    protected Long compute() {
        if (n <= 1) {
            return n;
        }
        Fibonacci f1 = new Fibonacci(n - 1);
        f1.fork();
        Fibonacci f2 = new Fibonacci(n - 2);
        f2.fork();
        return f2.join() + f1.join();
    }
}
