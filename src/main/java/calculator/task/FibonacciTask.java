package calculator.task;

import calculator.math.Fibonacci;
import calculator.proxy.Calculable;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by liyf on 2015/3/30 0030.
 */
public class FibonacciTask implements Calculable<String> {

    Integer n;

    public FibonacciTask(Map<String, List<String>> parameterMap) {
        n = Integer.parseInt(parameterMap.get("n").get(0));

    }

    @Override
    public String calc() {
        if (n > 40) {
            return "由于计算能力有限，您的数值过大，请输入小于等于40的数";
        }

        Fibonacci task = new Fibonacci(n);
        ForkJoinPool pool = new ForkJoinPool();

        pool.invoke(task);
        return task.getRawResult().toString();
    }
}
