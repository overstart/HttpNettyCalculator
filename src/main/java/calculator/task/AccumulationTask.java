package calculator.task;

import calculator.math.Count;
import calculator.proxy.Calculable;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by liyf on 2015/3/30 0030.
 */
public class AccumulationTask implements Calculable<String> {
    String s;
    String e;

    public AccumulationTask(Map<String, List<String>> parameterMap) {
        s = parameterMap.get("s").get(0);
        e = parameterMap.get("e").get(0);
    }

    @Override
    public String calc() {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        Count task = new Count(Integer.valueOf(s), Integer.valueOf(e));
        forkJoinPool.invoke(task);
        return task.getRawResult().toString();
    }
}
