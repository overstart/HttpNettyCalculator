package calculator.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by liyf on 2015/3/30 0030.
 */
public class CalcProxy<I> implements Calculable<I> {
    protected final static Logger logger = LoggerFactory.getLogger(CalcProxy.class);

    private Calculable<I> calculable;
    private static CalcProxy singleton;

    public CalcProxy() {
    }

    public Calculable<I> proxy(Calculable<I> calculable) {
        this.calculable = calculable;
        return this;
    }

    @Override
    public I calc() {
        long s = System.currentTimeMillis();
        logger.info("计算开始：");
        I i = calculable.calc();
        long e = System.currentTimeMillis();
        logger.info("计算结束:耗时 " + (e - s) / 1000.0 + "s");
        return i;
    }
}
